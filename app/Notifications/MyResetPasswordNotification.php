<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;


class MyResetPasswordNotification extends ResetPassword
{
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Você está recebendo essa mensagem, porque uma redefinição de senha foi executada.')
            ->action('Alterar senha', url(config('app.url').route('password.reset', $this->token, false)))
            ->line('Se você não solicitou uma reinicialização da senha, nenhuma ação adicional será necessária.');
    }
}
