<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 12/12/17
 * Time: 00:22
 */
function layout()
{
   return \Request::is('admin/*') ? 'layouts.admin' :  'layouts.app';
}

function isAdmin()
{
    return \Request::is('admin/*') ? true : false;
}